---
title: "Mnemonic doctor"
weight: 9100
---

TL;DR You can use midjourney to help you remember passwords

When using cryptographic accounts (by that I mean private keys derived from a seed),

We have to remember a random seed. We can rerand a seed by we cannot choose it ourself.

The goal is to make a "brain wallet" where we can remember our seed.


Let's take an example from padloc:

```
kite quarters hamstring food 
```

Or another one:

```
turtle elated floss mandate
```

<small>they seems surpisingly short to have enought entropy</small>


And some more challenging for cryptocurrency wallets (BIP39 mnemonic) :

```
pet sunny stuff
output roast pistol
duck entire pistol
dizzy elevator fossil
```

or

```
burger firm leave
anchor fence expose
boat segment yellow
slim furnace sort
```

If we could generate meaningful images along the mnemonic (1 or 2 images for padloc and 1 image for every 3 words for BIP39, it could help remember the seed !

/!\ The image should not be seen by anyone else, as it could reveal the mnemonic /!\
