---
title: "Messenger"
date: 2022-08-19T18:42:06+02:00
draft: true
---

At first sight, messaging activity could be done with Berty, "the decentralized Signal".

You could use your Safebook Account account to generate your Berty keys
