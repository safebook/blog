---
title: Vrac
---

# Bitmessage extension - Noise channel

AFAIK, we still leak informations when using bitmessage: That we sent a message, and the size of the message (since content, sender\* and destination stay unknown)

This is a proposol to replace this leakage by this one: That we are actually using bitmessage. Without telling what we do (send or not send a message, etc).

Purpose is to use it in a semi-decentralized fashion (by semi-decentralized I mean with monster-nodes in the network, that account for many operations).

Doing so make the protocol even less efficient and more restrictive. We assume that scaling bitmessage (the efficience problem) could be solved later.

Proposal: 1) Fixed message size (à la twitter). 2) No-knowledge throttled channels: Send a blob (meaningful if we really have one - or - full random) every epoch (ex: 1 minute)

# Bitmessage extension - Replacing proof-of-work by trusted peers

- There is a set of trusted public keys (aka (pseudo-anonymous) users)
- Every trusted user can add a message every epoch by signing it.
- If we find two message at the same epoch, the user is *slashed*, that means that a proof of abuse is sent to the network, and then the user isn't allowed to send messages for a fixed period of time.
- (Epoch could be some kind of merkle root of the bitmessage network, or else a decrentalized pulse or else a simple timestamp)
- (Invitation into the network is by web-of-trustish invitations like in Duniter)

# Idea of project: Bitmessage.js
