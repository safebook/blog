---
draft: true
---

*Alice et bob*
Alice---(hey)---Bob

*Image de communication entre alice et bob sur internet*
Alice
   \
    \
     (Internet)-----Bob

Que se passe-t-il quand Alice parle avec Bob ?

*Internet est fait de serveurs*
Alice---Serveur---Serveur---Serveur---Bob

**C'est un peu plus compliqué que ça :**
*Image de communication entre alice et bob*
Alice---FAI---Router---Router---Load-Balancer---Serveur(---BDD)---Load-Balancer---Router---Router---FAI---Bob

# Communication cryptés

*Une communication non chiffrée*

*Une communication chiffrée en HTTPS*

*Annexe: Le problème du MITM*
(Note: Certificate pinning is good, thanks Signal)

*Une communication chiffrée de bout en bout*

# Analyse frequentielle

**Attaquant sandwitch (Hebergeur ou FAI)**
*Communications + Tableau de corrélations*
