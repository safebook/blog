---
title: "Secure voting"
weight: 9000
---

One way to build secure electronic voting systems is to use mix-nets, which break any correlation between voters and their votes.
Another way is to use homomorphic encryption, where ballots are encrypted and only the tally is decrypted.

# Homomorphic encryption

Voters encrypt their vote and votes are added using homomorphic encryption. Only the result is decrypted.

There is a need for a guardian, which hold the keys to decrypt the result of the election.

The guardian could be an organisation, or even a quorum of people holding a fragment of the key. They need to get together to do a decryption.

A flaw is that if the guardians can decrypt the result of the election (which is a kind of sum of every votes), they can decrypt every encrypted ballot during or after the election.

# Mix nets

Votes are in plaintext, but we doesn't know who vote what. Anonimity is done through onion routing (or another anonimity strategy)

Voters can send their vote, using a anonymised voting token, securely delivered to everyone.

If you are worried about timing attacks, then let everybody vote at the same time anonymously using TOR.

One could event invent a discrete TOR (dTor) that operate synchronously et every mixing step, negating timing attacks.

## Sequential voting

You can see the evoluting state of the election (others vote) before making your vote.

This would be the case in a naive distributed mix-net base voting scheme.

## Synchronous voting

The result of the vote is only shown at the end of the election.

One implementation with two steps:

- Voting phase: where everyone send their hashed vote hash(vote, salt) associated with their voting token
- Result phase: everyone reveal their (vote, salt) [note: should be done using a mix network as well]

# Lotus mixing

With onion routing, at every step we know who sent us the packet, we decrypt it, and we know to who we should send it.

With n peers, when n is small, we can hide sent_by and sent_to with lotus mixing.

There is a (diffie-hellman) pki so we have a shared symmetric key with every peer.

We prepare *n* messages, one for each peer. Out of those n messages, 1 is a real encrypted message and n - 1 are random strings.

Every encrypted message is in the form (id,content,mac) where mac is the message authentication code and content is another encrypted to another peer until the final payload.

At every step, every peer:
- Read the list of (n * n) messages (n * n)
- Try to decrypt every messages with every shared key (by verifying the mac).
- When it is indeed intendeed to us, we decrypt it and forward it to the next person.
- When we cannot decrypt it, we just emit a random string instead.

At the final step we successfully mixed all messages

<!-- 
https://votenroll.cheredeprince.net/
-->
