---
title: Safebook
---

Safebook: Intially a hack to for end-to-end encrypted messages over facebook. Now random thoughs about cryptography
<!--
## Description

Safebook is a social network protocol.

It's a **free software** (free as in freedom). Everyone can use it, read the code and even modify it.

You really **own your data and account**, it's self-hostable and uses decentralized computing

It's **built for human rights**: It works in various network conditions, prevent surveillance and is uncensorable <small>(but you have the choice to use an algorithm to filter offensive content, enabled by default)</small>

## Why ?

- No third party, server or organisation (Code is law).
- Serverless. No centralized server.
- You don't have to trust the network or anyone (Trust is inherited from cryptography). Works in untrusted networks
- Works with or without internet access.

## Features

Step 1:
- No server-side stored account (improvement on matrix, signal, almost all social app, inspired by bitcoin HDwallet). Emphasis on decentralized id and ddns (directly used as usernames)
- Authentification and permission management is based on electronic signatures (like in bitcoin)
- Secure messaging is based on asymetric cryptography (light version of the Signal protocol)
- Distributed file storage bases on IPFS and optionally filecoin
- Static feed and personal webpage generator hosted on the decentralized web to allow a fast-accessed trusted (signed) profile (using decentrelized ids and decentralized domains, and hosting the website on the peer-to-peer web using IPFS and IPNS
- Bloom filters are used to avoid giving away rendez-vous points addresses (Research in secure information retrieval) TODO: add blogpost link
- Almost invisible messaging, think bitmessage, could be mixed with steganography TODO: add blogpost
- Safebook account securly make sub accounts for cryptocurrencies, if necessary in the futur

Step 2:
- Quantum resistant
- Ephemeral messages (time if unread, time after read)
- False pin code go to false account or delete all messages

Step 3:
- Machine learning using homomorphic encryption


## Public profiles : Lightning fast decentralized websites TODO: Merge with projects/light

Store your website on the decentralized web using IPFS

Authentificate your website by signing it with your IPNS private key

Build a static pages at compile time to make your website lightning fast

Step by step :

- The user add keys to his state (username, picture, various info, public friends).
- The user add messages to his feed.
- Using a choosen template, a static webpage representing the user profile is generated.
- The webpage is sent to IPFS and signed as a IPNS link

## Unbreachables accounts TODO: Merge with projects/account

There is no server-side account

At first there is the passphrase. (Safebook generate a cryptographically-strong passphrase for you. Some people may learn it by heart.)

Everything is generated from there. That means that if you login from a different device you're already good to go

We can derive :

- A signing key for the public profile, if necessary
- Multiples keys for storage (drive/photos)
- Multiples keys for private messaging
- A wallet addresses for bitcoin, ethereum, and other cryptocurrencies
- A password manager master key (wishlist) <span class="text-xs">(can support both classic hidden_passwords and deterministic passwords (ex: m/58/passwords/website_name))</span>
- PGP keys for compability with other tools (wishlist)
-->
