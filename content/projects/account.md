---
title: "Account"
weight: 20
---

[Visitez la démo ici](https://account.safebook.fr/)

**Usernames as public keys (a necessity for decentrelization)**

```
Un Compte pour les gouverner tous 
Une Graine pour les trouver
Un Chemin pour les amener tous,
Et dans la cryptographie les lier
```

- Support for vanity addresses !
- Support for DDNS (and building a new DDNS?)
- Printing accounts on T-shirts and accessories (cartes de visites, wearables, patch en tissus pour sac a dos etc) as pubkey QR code (eliminating the need for DDNS)
- Support for hardware wallets
- Support for shadow addresses (one-time addresses)

On utilise la sécurité de bitcoin pour générer :

- Des addresses de paiement (bitcoin, ethereum, etc)
- Des addresses pour la communication privé
- Un compte IPFS/IPNS
- Un générateur de mot de passes

Qui dit vraie sécurité dit la possibilité d'utiliser un Hardware Wallet. C'est un chantier en cours.

<!--

"One Seed to rule them all, One Key to find them, One Path to bring them all, And in cryptography bind them."

We use bitcoin cryptography to generate all the cryptographic accounts you'll never need (using hierarchical deterministic wallets (BIP32/44/85)):

- Bitcoin and \*coin keys (Classical path)
- Ethereum wallet (Classical path)
- Private/Public ECDSA and ECDH keys (Custom)
- IPFS/IPNS private key
- and even key for the **password manager**
- etc

Because paranoid accounts means a air-gapped computer or a hardware wallet, we want hardware integration (Ledger ? Status keycard ? or one of the many possibles hardware wallets ? or a air-gapped (without network connectivity) small computer that communicaute via QR codes ?)

-->
