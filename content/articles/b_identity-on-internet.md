---
title: "Identity on Internet"
date: 2022-08-13T15:17:36+02:00
weight: 30
---

What enforce that our names are ours ?

Before internet, communities and states did it.

On internet, we de deleguate our identity to autorities that maintain a registry.

For domain names (think: wikipedia.org), we trust [Domain name registries](https://en.wikipedia.org/wiki/Domain_name_registry).

For email addresses, email providers. For Facebook accounts, Facebook. Same with Twitter accounts, Instagram, etc.
<small>For HTTPS certificates, the backbone of internet security, we need to trust [Certificate authorities](https://en.wikipedia.org/wiki/Certificate_authority).</small>

This is by all means not a necessity. We can use cryptographic names. There is two kinds of them : **Hashes** and **Public keys**

## Hashes: Files identity

Peer-to-peer networks uses [Content addressing](https://en.wikipedia.org/wiki/Content-addressable_storage) to fetch content.

It's the opposite of location-addressing. It has numerous avantages:

- You don't need to trust anyone: The content you download is cryptographically verified to ensure that it hasn’t been tampered with.
- Even if the original publisher is taken down, the content can be served by anyone who has it.

The cryptographic hash functions is **deterministic** (on file always have the same, unique name) and **collision-free** (Two files have two differents hashes. Collisions are virtually impossibles).

**redondant:** The hash is the file's identity. After downloading the content we can run the same algorithm and verify the hash ourself.

Of course, it's not limited to files. Directories can use content-addressing as well. In this case the address (or name) would be some kind of merkle tree of the directory files and sub-directories.

## Public key: Users identity

Users are not like a static file. They can act. Proving that the act is done
by the user and not someone is is called **authentication**.

[Public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) (also called asymmetric cryptography)
can provide authentication with [digital signatures](https://en.wikipedia.org/wiki/Digital_signature),
proving to everyone that it was sent by the user (e.g. a social network post or a bitcoin transaction)

Public-key cryptography can also provide **private communication**. By knowing someone's public key,
I can send a message to that person that only him can decipher.

**That's why usernames should be public keys. Otherwise you it's not really ours.**

Like every computer data, public key can be interpreted as a huge number and is stored in binary.

### Annex: Decentralized Domain Name System (DDNS)

Here we have a registry of all the names but it is managed by a
[Decentralized Autonomous Organisation](https://en.wikipedia.org/wiki/Decentralized_autonomous_organization),
so it's still decentralized.

It's stored on a public decentralized ledger (in this case a blockchain) and you can pay to register a name.

- [Namecoin] (.bit)
- [ENS] (.eth)
- [Tezos Domains] (.tez)
- [Unstoppable domains] (.x, .crypto, ...)

[ENS]: https://ens.domains
[Namecoin]: https://www.namecoin.org
[Tezos Domains]: https://tezos.domains
[Unstoppable domains]: https://unstoppabledomains.com

### Annex: Sharing identity offline and with other apps: Encoding

*TODO: exemples should be 128 each (the signing public key size), to compare. Maybe different font sizes*

When human are involved, binary is not the most efficient representation, and we should choose carefully our encoding.

| Encoding             | Example         | Compression when printed  | Comment |
| -------------------- | --------------- | ------------------------- | ------- |
| Binary               | 101010101010    | Very bad (1 bit/char)     | The less compact text representation |
| Hexadecimal (Base16) | 0x37d95cd31     | Correct (4 bit/char)      | Used by ethereum addresses |
| Base64               | SGkgd29ybGQh    | Very good (6 bit/char)    | Used by ethereum addresses |
| Base58               | zr3474lcj8xss2f | Very good (5.86 bit/char) | Used by bitcoin addresses. More complicated to convert to binary, we have to rely on division and multiplication instead of binary operators |
| Mnemonic             | fold guess annual outside pelican vapor friend | Medium (11 bit/word) | Less compact but easy to share and remember
| Barcode              | ![Barcode](https://upload.wikimedia.org/wikipedia/commons/6/65/Code11_barcode.png) | Excellent                 | For machines |
| QRCode               | ![QRCode](https://upload.wikimedia.org/wikipedia/commons/7/78/Qrcode_wikipedia_fr_v2clean.png) | Incredible                | For machines |

