---
title: "Zero"
date: 2022-08-19T16:37:56+02:00
---

Une messagerie qui ne dévoile le contenu des communications (grâce à la cryptographie)
ni même que vous êtes en train de communiquer tout court (grâce à la stéganographie).

Safebook Zero peut apprendre à mimer n'importe quel site. Pour quelqu'un d'exterieur, il semblera que vous visitez le site en question,
alors que vous utiliserez Safebook Zero.

Le contenu de la communication sera caché dans ce qui semblera l'activité normale du site.

[Voir la démo](https://zero.safebook.fr/)

<!--

Every end-to-end encrypted messaging apps encrypt the content of messages.

[Safebook Zero](zero.safebook.fr) aims to be **metadata-free** as well.

It means that the sender, receiver, creation time, real size, shouldn't leak.

Safebook Zero is also **peer-to-peer**.

A pionner in this space is Bitmessage, but AFAIK it fails on scaling (Everyone has to copy the full database)

When implementing scaling, it's important not publicly advertising what messages you are interested in.

For that we'll look at [peer-to-peer private information retrieval](/posts/p2p-pir)

-->
