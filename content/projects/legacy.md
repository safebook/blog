---
title: "Legacy"
---

[Visit the site here](https://legacy.safebook.fr/)

Safebook started as a browser plugin for encrypting
facebook messages before they are sent and decrypting safebook messages when
found.

Quite similary of what mask.io do now.

Because facebook is listening to every keystroke (for bad and good reasons (ex: suggestions etc)) it is
not possible to inject a script in the page that hide messages after they have been typed without leaking informations, so the on-facebook encyption (and decryption) has been abandoned in profit of encryptiong on a separate page.

It was abandonned for before this new attemps, in a totally different direction.

New ideas are borrowed from other projects like bitcoin, bitmessages. etc.
Ex:  base58 encoding for address with SB prefix vanity addresses, shadow addresses, signing for publication,
and now aim to be server independant to allow different server implementations (and hopefully a decentralised one)
