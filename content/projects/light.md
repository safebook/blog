---
title: "Light"
weight: 10
---

Pour publier quelquechose sur internet, je n'utilise aucun intermédiaires. Je crée mon propre site.

Ainsi, j'ai le controle total de mes données, je ne dépend pas des GAFAM et il n'y a aucune publicité.

Voici comment je procède :

- J'écrit un message publique, un message privé, ou je partage une photo.
- Mon propre site internet est automatiquement modifié et partagé à tous mes amis.

J'héberge moi-même mon site ou je le fais herberger chez des amis, ou dans une association que je rejoint. Cela forme un réseau peer-to-peer, ou tout le monde parle directement à tout le monde.

Je peux rendre mon contenu virtuellement accessible à l'infini, sans dépendre de la bonne santé d'une platforme.

J'utilise la cryptographie pour communiquer de manière sécurisé (pour prouver que c'est bien moi et pour rendre impossible l'écoute de mes conversations)

# Rendre cela accessible à tous

Nous voulons promouvoir cet utilisation décentralisée d'internet contre les platformes traditionnelles de réseau sociaux.

Nous fournissons donc un logiciel permettant d'automatiser toutes ces taches pour utiliser internet de ce que nous considéront comme la bonne facon, tout en gardant la fluidité et l'experience qui nous rend tous accros.

# Comment modérer le contenu ?

- IA
- Karma

# Pile technique

L'utilisateur construit son profil dans une interface.

Ensuite nous utilisons un générateur de site static (ex: hugo) pour générer son profil.

Le site est ensuite hebergé sur ipfs (un réseau peer-to-peer), et on peut acheter un nom de domaine décentralizé (DDNS) pour avoir une addresse web facile à retenir.

L'utilisateur est identifié soit par sa clé publique, soit par son nom de domaine décentralisé.

Le concept est similaire à [ZeroNet](/related-projects#zeronethttpszeronetio).
