---
title: "Regulating the Uncensorable Web"
date: 2022-08-17T01:44:14+02:00
weight: 20
---

With [Safebook Light](light.safebook.fr), we plan to build a uncensorable web. The goal is freedom of speech.

TLDR: Filtering inappropriate content should be done by the end-user's softwares, not by platforms.

The uncensorable web is scary because of inappropriate content.

We have seen it happen last time with mastodon and hentai

When publishing to platforms, they have the power to regulate what content to keep and what content to delete.

On the distributed web things are differents.

All peers are equals so everyone should be able to filter.

## Measure 1 : Having (AI based) classifiers

Like spam detection.

We run them before seeing new content.

We could even train them through distributed training.

PS: Countermeasures (like randomisation, etc) should be taken against "advesarial machine learning exemple" (how to say?) (it means intentionnaly crafting a data
that will use weakness and bugs in the neural networks to make it be labelled the wrong way)

PS: Could use anomaly detection

## Measure 2 : Using the community to to approve or "signal" the content

In parallel of the graph of web data, we maintain a graph of data's reputation.

To do that we give users the power to approve or "signal" the content.

Content that is marked as offensive virtually disappear and always trigger a warning "This content is 'blocked', here is the reason: "

In safe mode mode, we can only see content that was approved by enough people, or by at least one person we know and trust.

We may even want *commities*(spelling? other name?), that are a group of reliable people that
can verify important content for the good of the community, sometimes for money.

Commity members are often publicly know people so there is a strong incentive to do the job well.

PS: We could also see recommandations, (or recommandations++) as a way to share content to people we know.
