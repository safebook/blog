---
title: "A worldwide PKI for phone numbers"
---

A database of public keys associated with phone numbers.

When you contact someone new, you can directly use end-to-end encryption.

But without revealing what phone number exists in the database (that is public).

It should start as noise (one fake public-key for every possible number) and be gradually updated, without revaling what is updated.

## Example size of the database

Let's look at every french mobile phone numbers.

It's start with 06 or 07 and has 8 more numbers (ex: 0612345678). That's `2 * 10 ** 8` = 200.000.000 phone numbers.

If every phone number is associated with 16 bytes of data (a typical X25519 pubkey, that makes a database of 3.200.000.000 bytes (3.2Go)

## Picking a random oracle

Every update of the database will mutate 1/n of the phone numbers. One way to build an oracle that will update every record with probability 1/128 is :

```
k = 7

# Generate the number with the k rightmost at 1 (and the rest at 0). Ex: n_bits(4) = 00001111
def n_bits(k):
  return (2**(k+1)-1))

def should_update(i):
  return (hash(seed + str(i)) & n_bits(k)) == 0

# Bruteforcing a seed
while 1:
  seed = random()
  if should_update(encode(phone_number_to_update)) == True:
    break
```

## Updating the database

A distributed solution is unknown.

But this could be done by an organisation that validate the "phone number"/"public key" association by receiving a self signed public-key by sms.

Then the organisation should update the database (every-time or by batches).

Every update will mutate 1/n records. n is a tradeoff between anonimity and update size. For `n = 2 ** 16 = 65536`, update size is 6.4Mo.

```
For every number:
Should we update the pubkey (given the oracle) ?
No: skip
Does we have a public key for the record ?
No: Let's just replace the actual value with a **new random value**
Yes: Let's **use the next public key** for this user.
```

We can generate as many public key as we want for that user thanks to [hierarchical deterministic wallets](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki).

But the user will have to try (at most) every public-key for every message, so the number of updates for a user should stay low (ex: 10 times)
