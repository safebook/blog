---
title: "On Signal"
date: 2022-08-13T14:48:53+02:00
weight: 10
---

# Signal: The state-of-the art privacy messaging app

Signal has been the go-to private messaging app for many years now, since the introduction of the Signal Protocol by Moxie and co, really.

It's a non-profit and the code is open source, so everyone can verify it. Since then, they are still pushing the boundaries of privacy, with the usage of Zero-Knowledge Proofs [n] for example


The protocol has been juged so good that he has since be adopted by other (closed-source) messaging apps like Whatsapp.

But for those, implementation details, side-channel attacks, and security by offuscation are still big concerns for privacy.


But by no means we should consider it to be perfect. There is always room for improvements. And Signal should be as open on his merits that on his flaws.

Let's share common concerns about metadata, centralisation, the usage of phone numbers, google play services on android.

After that, let's get pratical and have a quick look at how the application behave and get a glimpse of the data model.

# Concerns 

## Metadata

Media should be careful when they say that "the closest piece of information to metadata that the Signal server stores is the last time each user connected to the server, and the precision of this information is reduced to the day, rather than the hour, minute, and second" [source](https://theintercept.com/2016/06/22/battle-of-the-secure-messaging-apps-how-signal-beats-whatsapp/), it's a dangerous claim, when potential lifes depend on it. (And wikipedia should be careful relaying it)

I claim that there is a lot more to the metadata story.

1. I see that you're using Signal
First, to use Signal, one has to etablish a connection with the Signal's servers. From mobile antennas to underwater cables, operators knows that you're using Signal. This is the very first piece of metadata.

2. States may guess with who you're talking
I claim that timing based desanonymisation is possible

3. When you speak of anonymization, you should consider on-the-wire data, not what you store on the database.
Yes anonimity of the database is important, if it leaks. But many things can go wrong before writing to the database.
(ex: Contact discovery)

Let's try to understand Signal's internal.

## Anonymat

- Coupled with phone number (even in the form of truncated hash)
- Group id stay constant (to verify)
- Vulnerable to sandwitch statistical timing desanonymisation if there is a MITM for Alice AND Bob (for exemple if they share the same FAI)

(All of this claims are fixed by our protocol)

## Recherche de contacts

https://signal.org/blog/contact-discovery/
https://support.signal.org/hc/fr/articles/360007061452-Signal-envoie-t-elle-mon-num%C3%A9ro-%C3%A0-mes-contacts-

## Les numéros de téléphone

Il semble que les numéros de téléphone soit **presque obligatoire** pour utiliser Signal.

Signal utilise Twilio pour vérifier le numéro par SMS.

(https://support.signal.org/hc/fr/articles/4850133017242-Incident-Twilio-ce-qu-il-faut-savoir)

## Décentralisation

The Berty Protocol [n1] is the Signal Protocol on a distributed fashion. It provides a local-first, peer-to-peer, "no single point of failure" infrastructure.

But we can ask, now that the social (encrypted) data is shared in the open, is it not easier for any actor can analyze it for every once of statistical information about metadata ?

Moxie said at a CCC conf [n2] that there is no plan to allow other Signal servers

# Pratical exercise: app analysis

I first used wireshark to analyze how Signal behave on the network.
*I've been looking at Signal-Desktop. Android and iOS version haven't been tested but should be similar.*

![Capturing Signal traffic](/signal-capture.png)

Unsuprisingly, it starts with a DNS query

We contacted 3 servers

```
 dig chat.signal.org
chat.signal.org.	137	IN	A	76.223.92.165
chat.signal.org.	137	IN	A	13.248.212.111
```

Which are Amazon's AWS Global Accelerator addresses.

```
 dig cdn.signal.org
cdn.signal.org.		121	IN	CNAME	d83eunklitikj.cloudfront.net.
d83eunklitikj.cloudfront.net. 57 IN	A	13.32.145.80
d83eunklitikj.cloudfront.net. 57 IN	A	13.32.145.48
d83eunklitikj.cloudfront.net. 57 IN	A	13.32.145.30
d83eunklitikj.cloudfront.net. 57 IN	A	13.32.145.126
```

```
 dig storage.signal.org
storage.signal.org.	103	IN	CNAME	ghs.googlehosted.com.
ghs.googlehosted.com.	206	IN	A	216.58.198.211
```

Let's look at SSL certificates. Here is the [one for chat.signal.org](https://www.ssllabs.com/ssltest/analyze.html?d=chat.signal.org&s=13.248.212.111) of chat.signal.org

It's self-signed by Signal and the Certificate Authority is [in the source code](https://github.com/signalapp/Signal-Desktop/blob/main/config/default.json#L27)

It's a form of SSL pinning and it's very good, because we don't have to worry about malicious Certificiate Authorities (CA).

| Server | Host | Certificate |
| -- | -- | -- |
| chat.signal.org | AWS Global Accelerator | Signal |
| cdn.signal.org | AWS Cloudfront | Signal |
| storage.signal.org | Google App Engine | Signal |

On the good hand all the certificates are generated by Signal which is a very good news. We always have a direct cryptographic tunnel to Signal's servers.

On the other hand it's hosted by Amazon or Google, where the US government could probably access the machine and read the certificate private keys.

## Without knowing the TLS certificate: What ISP governements, and hackers can know

1. They know that you are using Signal.
It's obvious because you made the DNS queries and maybe because of the IP addresses you're contacting.
This sounds stupid but it's sometimes enough to stigmatize you if - for exemple - liberty of speech is not well seen in your country. 
Using a VPN resolve this, if you trust you're VPN

TODO: Make a plot of packets over time, when sending a message, etc, with a different apparence
when sent/receive small/big dependending on peer (chat/cdn/storage)

TODO:
- Do they know when you're sending a receiving a message ? (Local attackers)

- Is read-receipt, for exemple, easily reconocable ?

- Could a global attacker (that listen to your, and your friend's traffic) know more by analyzing the traffic,
for exemple who you are talking to,
and by manipulate the traffic (by delaying packets for exemple) ?

## After TLS

I used [mitmproxy on transparent mode with the workaround for traffic originating from the machine itself](https://docs.mitmproxy.org/stable/howto-transparent/)

ex: https://cdn.signal.org/profiles/oeJwAgTBPrny6CjJXxDDdQ

```
user-agent:Signal-Desktop/5.55.0-beta.1 Linux 5.15.59-1-MANJARO*x-signal-agent:OWD
```

# Controverses

## Phone numbers

## Google play services

## Centralisation

601	151.233084254	192.168.0.25	76.223.92.165	WebSocket	434	WebSocket Binary [FIN] [MASKED]

GETq/v1/profile/1434a8ab-de2a-4df2-b459-39b49c91f071/3dc310811e5c5511e9438b25cebd3203dd0edbd71f048d3a76367c06c8f1594d *accept-language:en-US*,content-type:application/json; charset=utf-8*0unidentified-access-key:6SGSN8WZK9ZT7i6NpsidwQ==*?user-agent:Signal-Desktop/5.55.0-beta.1 Linux 5.15.59-1-MANJARO*x-signal-agent:OWD

«
ÈOK"ë␌{"identityKey":"BdSiQhhtYADYhOy2mRrMnNU2bQVr+B8MkrFtsq3t0q1K","unidentifiedAccess":"c9BHyxnMumF1d5ej4iGFOzaZL72/hI832Rwd0n94L0A=","unrestrictedUnidentifiedAccess":false,"capabilities":{"gv2":true,"senderKey":true,"announcementGroup":true,"changeNumber":true,"stories":true,"giftBadges":true,"gv1-migration":true},"badges":[],"uuid":"1434a8ab-de2a-4df2-b459-39b49c91f071","name":"21WbaiissX2R3rjjdRiIP7T5Lz1B/7qyxDEFt7zRik4o7BfzUqBRMve1G/SDct3EcEJ3C0HMl/NJ6aHoLcH+auAvUCpwOD2yH6e1lH04JEuM","about":"T7NLwUyuIqLsfT4pR8X8OYFV5AZB6US3dbhoHXKzWn6OBguczR2pudVjJKO7UuTj5MAyaAXhUftUn+cakZMlTTDCuPOgx93Re1p7Z0e8gjv0nL376tbLpSuATFFoJ4EYnwdejWvLk8OZx41hGMp8T4dB18b3tab+Cf1+b5yeILyc1CpOmSUpBp3Uf+WuScVlXffaT+bJSja1+RBn","aboutEmoji":"QNFBC6s2F+zxEERa/t1fsk7I6NTc+gOJb3u36doUXdrEfBpeIK5dBrOlqBa/ush/f90i1/s9Sbro2sZz","avatar":"profiles/Y0WSr_R5_1aIEu3A4orQdw","paymentAddress":"9mjlhsz3cRiWA2Z08jPg2x9Y3yy03T9FksfrMt5/trBNQfvmAajHWnez8194SZw+kHK0eQyFQImHT9hYG60GI2HHVUN6xHRAbMLnq8tdo6tHDmU6EkMU5a5Q2V7/whrKDX8Nc17zsQlAIAa1XVAIl8ska6MTj91wci6/MAH7vRlle30ixsOv+/JYvGl6KCSMKGkvOpeErs6yjsrLH6ItIo/2XccF526i7viCykBPZQmoFiw6srYFS3xWKk9j1PJpJXcZEjWq4t4xYyOgy9eFj2nSFc2x5w31Sy3wbQ3X4cuRYxpcDVHSsZo1o5C1bqyFNvdqeoafiynRlP3rwjb3oLLg0UK4RGox0dpbRqcVivrdQ8Q/X799enX+N3BjAVDDzWMhs0iYeXNY0MUadJhM/xHExY1jI7gMjIYO1+qwri1vT1GlxJPYf5IL/V/Mz6D7yaGfV9GYM/HuO4uZ0ooK4FO8o1cR0m/ZUdDW9s51Vx6BenRDFHbqItJvyE7W4j0icnzDp7FibfcdmaeUdRYZzgMuDFCmXdqYH70y7uF73A8apqunVtoGxOobEFF1s9pvm0S4u7+M72c/scuK8GkTncEk/M687KU3CHp/YFQs6FwH5k2JYVyAgOeqbRAOMX/29QEKq3zYv8hk64Q6D2wNdtzwDwwqbwNP3keqp1m0qlC5ulYu7pkQgqGYJMe/tVgGDFijsahTXrp8wOO8ZOAJ0XzhoTYf9gUZht4UtA54D1WL+HUxxl85UvsrKbEQGmUByKnalbc3"}*Content-Type:application/json*Content-Length:1643

Ü␌
PUT1/v1/messages/1434a8ab-de2a-4df2-b459-39b49c91f071²
{"messages":[{"type":3,"destinationDeviceId":3,"destinationRegistrationId":65,"content":"MwgDEiEFE4pKz927hOpcG1Rp0HbyrFtwjIorVZnDiq1CJ1503HMaIQXUokIYbWAA2ITstpkazJzVNm0Fa/gfDJKxbbKt7dKtSiLzAjMKIQWngJGLnPB9Hfye+au/TMs6hul4SY8dWaMD26vJQ/A/DRAOGAAiwAIO6OS1P8MTlnj85GZ3WBR/XuM9wBnPn6tB9p7VN1EcDrQWqmb3niKHeZ2vwSLYQuPfPxmS0PasciIj0qa5xzXSn65bzfv32HWsCekm8EmWnAAqv9j4FqCsO4hcT0JmIEyulAk3M9Wy5+ST+TaUMssTr5tA95rxDYfw8VsZeQ9WLMaeOaqHXRHv7t6VZwjV8qKffnHIXfHoPj52xkeEOWkCMII15UAbqIokAzWURqgIzARv5HcOIhBZtr1KWyu1ODT7f+pfBW0SeU4pa6Ix8F3kLmIkTYCEdL+GikMam1olZo8+LtEauRkWNcXJRRytMjjwc6EdbqEqKOYreIKrxWtWrdBldl1fIHQgkSZRAgF3R7nqAQUsr4cbEHsTcr1c/pTecLtzhV9bH/K6PH/EzeLIZmuRsIqZlcNB7iY5A4m1Fx7tger2Vx5sKPVOMAE="},{"type":1,"destinationDeviceId":1,"destinationRegistrationId":1866,"content":"MwohBfNONkp61UUe6eX8YVpoVDFRmfq9UtQPlhT4BCc/CwVrEAUYACLAAqaxX5ihX3xylinFDgW1GSJxH1rrnHRrEJ1hLj5xLymAfP4ftzxUrL9p1KNnEMRA2ctLe9Cjx6iVoAX+7CpKTQD+df1DYQIvEYsYckX0hhrd1ofUk+FELAlee6j8JPEZzVvPbp3TBn3EsAl7wgtMUhu2iQNYr+8WwVwBg0rvkiXMXoaDGqGrSO7kqeBE5/1yb99qZxT6rIxOgWQsjXktsdWo6WYp6Qh9fSZWqnMHNy60XskwdaW024kVPgGTmRVk8rb01K1BOqeVVBZvlrhjIqEwtv7yUAcGwto48uvLDVlbBcyFwwAOMQBxq9es+TngT0H7ttgpGpeihlgXSJrqN96gzmLBgo2tqdGYOunCbB0eeW3i5cj/j9ZSGmnFCr+tbCVvpVXk6vWoZcMjFvUyozcwcC4InTxV8oR+o6QrEo94xjQ0kxvAXmI="}],"timestamp":1660440336809,"online":false,"urgent":false} ␋*hauthorization:Basic MTQzNGE4YWItZGUyYS00ZGYyLWI0NTktMzliNDljOTFmMDcxLjQ6R0VGd2ZwSnJkVkljbnJGVzFpUWN4Zw==*,content-type:application/json; charset=utf-8*?user-agent:Signal-Desktop/5.55.0-beta.1 Linux 5.15.59-1-MANJARO*x-signal-agent:OWD

# Conclusion

If you're interested in Signal's internal, be sure to also check the [wikipedia page](https://en.wikipedia.org/wiki/Signal_(software)) and the one of the [Signal protocol](https://en.wikipedia.org/wiki/Signal_Protocol).

