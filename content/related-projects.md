---
title: "Related projects"
draft: true
---

## GNUnet

## Signal

This one is very popular.

## Silence

Silence is a fork of Signal when it was still called TextSecure.

Compared to Signal, it is less intrusive:
- Everything is sent through SMS/MMS. There is no central server.
- It does not require Google Play Services (used for push notifications)
- It's not working on iOS (because of technical limitations)

It can be used as a replacement for the SMS/MMS android app.

It's not interoperable with Signal.

## [Status](https://status.im)

**Amazing app** combining an peer-to-peer end-to-end encrypted messaging app with an Ethereum wallet and more.

## Ring

[Ring](https://archive.fosdem.org/2018/schedule/event/ring/) is a peer-to-peer distributed communication platform
that is built on [opendht](https://github.com/savoirfairelinux/opendht), by the same developers.

DHT is one of the must useful P2P data structure, used heavily in IPFS or for trackerless bittorrent.

## IPFS

Provide a P2P network similary of bittorrent.

Promote [content-addressing](https://en.wikipedia.org/wiki/Content-addressable_storage)

Check the [IPFS primer](https://flyingzumwalt.gitbooks.io/ipfs-primer/)

## [Tox](https://tox.chat/)

## [Zeronet](https://zeronet.io/)

The uncensorable web. Not a messaging app, but a website publishing tool.

Websites are hosted on the BitTorrent network.
After you visiting a website you start sharing it as well.

Very similiar to the public side of safebook, that uses IPFS and IPNS instead of Bitorrent, and support more domains (additionnally to Namecoin (.bit), we support .eth (ENS), .x (Unstoppable domains), ...)

Unfortunatelly the project is not active anymore.

### [Secure Scuttlebutt](https://scuttlebutt.nz)

A gossip peer-to-peer protocol.

Feeds are represented as a hashchain of signed messages.

### [Berty](https://berty.tech)

A truly peer-to-peer, uncensorable and offline first messaging app.<br />
Based on IPFS

### [Bitmessage](https://bitmessage.org)

A radical no-metadata messaging app. Every message is a hidden blob that
you will try to decipher with all your keys, if you succeed, the message
is for you. (Scaling is a issue)

TOR-routing of messages is also offered.

### [Lens](https://lens.xyz)

A social network on a blockchain (Polygon).

Personnal consideration : I don't know if a pure on-chain approch could
scale well, every validator would have to validate every post, comments,
likes, ...

<small>
  See : <A href="https://lenster.xyz">lenster.xyz</A>, <A href="https://mirror.xyz">mirror.xyz</A>
</small>

### [Ceramic](https://ceramic.network)

https://blog.ceramic.network/tiles-a-browser-for-open-source-information/

### [DAT](https://www.datprotocol.com)

### [Retroshare](https://retroshare.cc)

### Freenet, I2P, Zeronet

### [Perfect Dark](https://fr.wikipedia.org/wiki/Perfect_Dark_(P2P))

### Retroshare

### PGP (and GPG)

PGP is the reference.
It's quite old but it provides state-of-the-art cryptographic. difficult to use

### Minilock, the metadata-free format

### Keybase

A web-of-trust implementation


## Interesting technologies

### Zero-Knowledge proofs

https://twitter.com/LuozhuZhang/status/1530984087061929984?s=20&t=_6d66haeswHBHNn4cEB46A

https://research.protocol.ai/sites/snarks/

### Crypto libraries

libsodium, nacl, tweet-nacl
sjcl
celle de l'auteur de real world crypto
openssl
