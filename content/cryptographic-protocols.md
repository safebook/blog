---
title: "Cryptographic Protocols"
---

# Peer-to-peer private information retrieval

[Private_information_retrieval](https://en.wikipedia.org/wiki/Private_information_retrieval)

## Truncated hash

Length should be taken to always have at least k collisions for having [k-anonymity](https://en.wikipedia.org/wiki/K-anonymity)

## Tree of interest

*It's a compression of truncated hash, isn't it?*

The tree of interest is a way to advertise what range of addresses we are interested in without sharing the exact addresses.

Imagine that we are interested in the following addresses :

```
- SBkj4vRoCGJym3xR7yCVPFHoCNxv4Twseo
- SBkrist8KkaUXSPKZHNvEyfrEkPHzSsCdd
- SBjJfcfHPXYJreMsASk2jkn69LWEYKzexb
- SB4319KR5vEneNb47ewrPfWyb5jQ2DjxRP
- SB4NXuVSEpWW4trkfmvWzegTHQt7BdktSK
- SB7V6bAHb8ybZjqQMjJrcCrHGW9sb6uFz6
```

We can build the following tree of interest

```goat
.               S                            .
                |
                |
                B --.
               /|\   \
              / | \   \
             /  |  \   \
            /   |   \   \
           k    j    4   7
          / \       / \
         /   \     /   \
         j   r     3   N
```

Or in short notation:

`SB{k{j,r},j,4{3,N},7}` 

We can make it a little more precise, and for convinences use leaves at the same height:

`SB{k{j4,ri},jJf,4{31,NX},7V6}`

Which is more efficient by 2 orders of magnitude.

The we could even add noize which would filter 2 or 3 times less result but provide better anonimity:

`SB{32r,Tk{1,8},k{b2,j4,ri,kk},j{21,Jf},2tj,4{31,kH,NX},5k{3,l},7V6}` 

*When we add noize, we should try to add it deterministically to be consistent and avoid giving away what is noize and what isn't*

## Bloom filters

Etudier et voir si on peut utiliser directement un filtre de bloom et quels sont les avantages/incovenients

**TODO: Rewrite**

Un filtre de bloom est une structure de donnée probabilistique permettant de savoir rapidement si un élément appartient à l'ensemble.
Un risque de faux positifs existe, mais pas de faux négatifs.

Ils sont utilisés pour savoir rapidement si un élément apparitent à un ensemble (ou plus exactement si il n'apparitient pas à l'ensemble, à cause du risque de faux négatifs).

Mais un autre usage peut-être l'anonymité de l'utilsateurs. En demandant à un tiers (peer ou server) si il possède des messages appartenant à ce filtre de bloom, on ne dévoile pas les messages que l'on cherche. (It can be used for exemple to search group ids)

Query: 0b0101001011

Answer: { total: 7, results: [{ msg }, { msg_2 }, ...]}

Si le filtre n'est pas assez précis (on a trop de réponse), on peut passer sur un filtre plus précis, mais cela fait perdre en anonymité. \*

\* On peut imaginer une attaque ou le pair renvoi toujours un grand nombre de faux message pour faire baisser le niveau de compression du filtre de bloom

## Interest oracle

*This is probably inferior to bloom filters and when I wrote this I didn't fully understood bloom filters. Maybe there is still interesting ideas but lets just use Bloom filters for now*

An interest oracle is a function that allow someone to filter the data that may interess him from the rest, without revealing what we are looking for.

### Context

When messages have no metadata, and are purely indinsiguishables from random, it could be vital to hide what are the users, groups and messages that we are interested in.
Otherwise an attacker could extract some informations if he knows what are the messages that we ask for.

(It is inherently different from purely replicated distributed data structures like blockchains, the content is too big to be replicated to everyone,
so we need to ask peers for interesting data. Attackers could pretend they are normal peer and reveal what we ask for).

*We should stay careful that, even when trying to minimise the shared information like this, attacks remains that can still reduce or even void anonymity. Further analysis should be done*

### Technical

It's a (simplified) kind of bloom filter: The oracle says either *The input may belong to the set* or *The input surely doesn't belong to the set*.

A proposed implementation is bruteforcing a salt that provides f(x) = 0 for every x in the set, with f(x) = LSB(hash(salt + x)) (where LSB takes the Least Significant Bits).

Depending on the size of the set, one can bruteforce an oracle. When the set grows it is exponentially hard to find one.

A 1-bit oracle filter one half of the inputs and bruteforcing one require 2^n try on average.
A 2-bit oracle filter three quarters of the inputs and require 4^n try on average

Mixing 2 1-bit oracle and asking at least one to be true filter 1/4 of the inputs but may be easier to compute

How difficult is it to bruteforce 5 1-bit oracles and asking at least 4 to be true ? What about 2-bit oracles ?

### TODO

- Investiguate if trapdoored hash functions exists, where the creator has some information about the underlined structure,
but the information about the structure is lost when given just the salt.

## Rendez-vous point

See **The precomputation approach of anonimity**.

When Alice knows Bob and Bob knows Alice, they both have ECDH(Alice,Bob). They can derive a RDV point only known to them.

## Anonimity set preserving hash functions

We are looking for a hash function giving the same result for all ids generated in the same anomimity set.

(Euh just provide a bloom filter of the anonymity set ?)

## Web-of-Trust building (see keybase)

A company or system for quickly building a web-of-trust. for exemple
associating a pubkey to a phone number. You could get tokens or money to
verify someone's else number

Ex: WoT for associating a pubkey to a phone number.

## Phone-number PKI
 
How to discover your friends on the network (using phone numbers for
exemples) without giving away your friendlist.<br />
Part1: Associate your phone number, email address or other
identification mean with a public key. This could be done using
web-of-trust. (This is a topic on its own)<br />
Part2: Compute a kind of "secret rendez-vous point" (H(K_a^k_b) ==
H(K_b^k_a)) from your public key (eg: associated with your phone number)
with all your friends "phone-numbers" public key and publish it<br />
Part3: Finding a published token linking our phone number to someone's
else means that our this is indeed our friend's public key<br />
[It is true indeed that after Part1, we don't need Part2 and Part3
because we already know our friends public keys]

## Secret Poke

We want to be able to "poke" or "like" a person in a serverless setup
without making the information public.

Step 1: Compute the ECDH bonding with the person that you want to poke or
like

Step 2: Compute the beacon. <code>Beacon = Hash(ECDH shared key)</code>

Step 3: The person B can now see if it is liked or poked by the person A
by computing the same beacon and checking if it exist in the network DHT.
